This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Below you will find some information on how to perform common tasks.<br>
You can find the most recent version of this guide [here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).

## Diagram
![Dagram](/backend-game/Diagram.png)

## Folder Structure

After creation, your project should look like this:

```
react-game-app/
  README.md
  backend-api/
    app/
        config/
        controllers/
        models/
        routes/
    node_modules/
    server.js
    swagger.json
  react-app-v002/
    node_modules/
    package.json
    public/
        index.html
        favicon.ico
    src/
        components/
            game/
            log/
            user/
            Header.js
        services/
            GameService.js
            LogService.js
            UserService.js
        App.css
        App.js
        App.test.js
        index.css
        index.js
        logo.svg
```

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

## Clone Repository
```
git clone https://gitlab.com/wahyu007/react-app-game.git
```

## Backend
```
cd backend-game
```

```
npm install
```

```
npm start
```

## Backend Running On

`http://localhost:8080/api/v1/users`

## Swagger Documentation

`http://localhost:8080/docs`

## Installing a Dependency of ReactJS
```
cd react-app-v002
```

```
npm install
```
## React Running On
`http://localhost:3000/user`
