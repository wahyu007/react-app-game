const express = require("express");
const bcrypt = require('bcryptjs');
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();
const swaggerJSON = require('./swagger.json')
const swaggerUI = require('swagger-ui-express');

app.use(cors());

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({
    extended : true
}));

app.get('/', (req, res) =>{
    res.send({
        status : "oke"
    });
});

app.use('/docs', swaggerUI.serve, swaggerUI.setup(swaggerJSON));

const db = require('./app/models');
require('./app/routes/routes')(app);


function create_roles(){
    db.Role.create({
        id: 1,
        name: "USER"
    });

    db.Role.create({
        id:2,
        name: "ADMIN"
    });

    db.User.create({
        name: "admin",
        email: "admin@admin.com",
        password: bcrypt.hashSync("123456", 8),
        address: "Jakarta",
        sex: "Male",
        phone: "0298128102"
    })
    .then(users => {
        db.Role.findAll({
            where: {
                name: 'ADMIN'
            }
        })
        .then(roles => {
            users.setRoles(roles)
        });
    }).catch(err=>{
        console.log(err)
    });
}

db.sequelize.sync({
    // force : true
}).then(() => { 
    // create_roles();
    app.listen(8080, () => {
        console.log(`Server is running on port 8080`)
    })
})

// app.listen(8080, () => {
//     console.log(`Server is running on port 8080`)
// })
