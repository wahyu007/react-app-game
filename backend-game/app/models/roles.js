// module.exports = (sequelize, Sequelize) => {
//     const Role = sequelize.define("user_game_role", {
//         id : {
//             type : Sequelize.INTEGER,
//             primaryKey: true,
//             allowNull: false,
//             autoIncrement: true
//         },
        
//         name : {
//             type : Sequelize.STRING
//         }
//     });

//     Role.associate = function(models){
//         Role.belongsToMany(models.User, {
//             through: 'user_roles',
//             foreignKey: {
//                 name: 'roleId',
//                 type: Sequelize.INTEGER
//             },
//             otherKey: 'id_user'
//         })
//     }

//     return Role;
// }

'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Role extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Role.belongsToMany(models.User, {
        through: 'user_roles',
        foreignKey: 'roleId',
        otherKey: 'user_id'
      })
    }
  };
  Role.init({
    id : {
        type : DataTypes.INTEGER,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true
    },
    name : {
        type : DataTypes.STRING
    }
  }, {
    sequelize,
    modelName: 'Role',
  });
  return Role;
};