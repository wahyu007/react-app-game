// module.exports = (sequelize, Sequelize) => {
//     const Log = sequelize.define("user_game_history", {
//         id_user : {
//             type : Sequelize.INTEGER
//         }, 
//         status : {
//             type : Sequelize.TEXT
//         }
//     });

//     return Log;
// }

'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Log extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Log.init({
    id : {
        type : DataTypes.INTEGER,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true
    },
    id_user : DataTypes.INTEGER, 
    status : DataTypes.TEXT
  }, {
    sequelize,
    modelName: 'Log',
  });
  return Log;
};