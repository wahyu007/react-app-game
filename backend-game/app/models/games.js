'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Game extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Game.init({
    id : {
        type : DataTypes.INTEGER,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true
    },
    id_user : DataTypes.INTEGER,
    score : DataTypes.INTEGER, 
    top_score : {
        type : DataTypes.INTEGER,
        allowNull: true
    }
  }, {
    sequelize,
    modelName: 'Game',
  });
  return Game;
};