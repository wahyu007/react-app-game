const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const db = require('../models/index');
const User = db.User;
const Role = db.Role;
const Op = db.Sequelize.Op;

exports.create = (req, res) => {
    if(!req.body.name) {
        res.status(400).send({
            status : "error",
            message : "Content can not empty"
        });

        return;
    }

    const user = {
        name : req.body.name,
        email : req.body.email,
        password : bcrypt.hashSync(req.body.password, 8),
        address : req.body.address,
        sex : req.body.sex,
        phone : req.body.phone
    }

    User.create(user)
    .then(data => {
        res.send(data);
    })
    .catch(err => {
        res.status(500).send({
            status : "error",
            message : err.message || "Some error Ocured while creating the user"
        })
    })
}


exports.signup = (req, res) => {
    if(!req.body.name) {
        res.status(400).send({
            status : "error",
            message : "Content can not empty"
        });

        return;
    }

    const userData = {
        name : req.body.name,
        email : req.body.email,
        password : bcrypt.hashSync(req.body.password, 8),
        address : req.body.address,
        sex : req.body.sex,
        phone : req.body.phone
    }

    User.create(userData)
    .then(user => {
        // res.send(user);
        Role.findAll({
            where: {
                name: "USER"
            }
        })
        .then(roles => {
            user.setRoles(roles).then(()=>{
                res.status(200).send({
                    auth: true,
                    id: req.body.id,
                    message: "User registered successfull",
                    errors: null
                })
            })
        })
    })
    .catch(err => {
        res.status(500).send({
            status : "error",
            message : err.message || "Some error Ocured while creating the user"
        })
    })
}

exports.login = (req, res) => {
    const email = req.body.email;

    User.findOne({ where : { email : email} })
        .then(data => {
            var passValid = bcrypt.compareSync(req.body.password, data.password);
            console.log(passValid);
            if(passValid){
                var token = "KEY " + jwt.sign({
                    id : data.id_user
                }, "secretAuth", {
                    expiresIn: 86400
                });

                res.send({
                    status : "success",
                    message : "Login successfully",
                    tokenAccess: token 
                });
            }else {
                res.send({
                    status : "error",
                    message : `password error`
                })
            }
            
        })
        .catch( err => {
            res.send({
                status : "error",
                message : `email ${email} not registered`
            })
        })
} 

exports.findAll = (req,res) => {
    const name = req.query.name;
    var condition = name ? { name : { [Op.iLike]: `%${title}`}} : null;
    User.findAll({ where : condition })
    .then(data => {
        res.send(data)
    })
    .catch( err => {
        res.status(500).send({
            status : "error",
            message: err.message || "error"
        });
    });
}

exports.findOne = (req, res) => {
    const id= req.params.id;

    User.findByPk(id)
        .then(user => {
            res.send(user);
        })
        .catch(err => {
            res.status(500).send({
                status : "error",
                message : err.message | "error"
            });
        });
}

exports.search = (req, res) => {
    const name= req.params.name;

    User.findAll({
        where: {
            name: { [Op.like] : `%${name}%`}
        }
    })
    .then(user => {
        res.send(user);
    })
    .catch(err => {
        res.status(500).send({
            status : "error",
            message : err.message | "error"
        });
    });
}

exports.findUserProfile = (req, res) => {
    console.log(req.userId);
    User.findByPk(req.userId)
        .then(user => {
            if(user != null){
                res.status(200).send({
                    status : "success",
                    data : user
                });
            } else {
                res.send({
                    status : "error",
                    data : "User Not Found"
                });
            }
            
        })
        .catch(err => {
            res.status(500).send({
                status : "error",
                message : err.message | "error"
            });
        });
}

exports.update = (req, res) => {
    const id = req.params.id;
    if(('newPassword' in req.body) && req.body.newPassword !== ''){
        req.body.password = bcrypt.hashSync(req.body.newPassword, 8)
    }
    
    User.update(req.body, {
        where: {id_user : id}
    })
        .then(num => {
            if(num == 1){
                res.send({
                    status: "success",
                    message : "User was Updated"
                });
            } else {
                res.send({
                    status : "error",
                    message : `Cannot update user with id = ${id}`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                status : "error",
                message : "Error updating user with id " + id
            })
        })
}

exports.delete = (req, res) => {
    const id = req.params.id;

    User.destroy({
        where : { id_user : id}
    })
        .then(num => {
            if(num == 1){
                res.send({
                    status : "success",
                    message : "User was deleted successfully!"
                });
            } else {
                res.send({
                    message : `Cannot delete User with id = ${id}`
                })
            }
        })
            .catch( err => {
                res.status(500).send({
                    status : "error",
                    message : "Could not Delete User with id ="+id
                })
            })
}