const db = require('../models/index');
const Game = db.Game;

exports.create = (req, res) => {
    const game = {
        // id_user : req.userId,
        id_user : req.body.id_user,
        score : req.body.score,
        top_score : req.body.top_score
    }

    Game.create(game)
    .then(data => {
        res.send(data);
    })
    .catch( err => {
        res.status(500).send({
            status : "error",
            message : err.message || "Some error ocured while creating the game."
        });
    })
} 

exports.findAllGame = (req, res) => {
    Game.findAll()
        .then(data => {
            res.send(data)
        })
        .catch(err => {
            res.status(500).send({
                status : "error on games",
                message : err.message || "Some error while retriving games."
            })
        })
}

