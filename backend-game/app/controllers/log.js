const db = require('../models/index');
const Log = db.Log;
const Op = db.Sequelize.Op;

exports.findAllLog = (req, res) => {
    Log.findAll()
        .then(data => {
            res.send(data)
        })
        .catch(err => {
            res.status(500).send({
                status : "error",
                message : err.message || "some error while retriiving data"
            })
        })
}

exports.create = (req, res) => {
    Log.create({
        // id_user : req.userId,
        id_user : req.body.id_user,
        status : "Status is " + req.body.status 
    })
    .then(data => {
        res.send(data);
    })
    .catch( err => {
        res.status(500).send({
            status : "error",
            message : err.message || "Some error ocured while creating the game."
        });
    })
} 