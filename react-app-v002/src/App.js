import React, { Component } from 'react';
import Game from './components/game/Game';
import User from './components/user/User';
import Log from './components/log/Log';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Header} from './components/Header';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <Header></Header>
          <Switch>
            <Route exact path="/game" component={Game} />
            <Route path="/user" component={User}/>
            <Route path="/history" component={Log}/>
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;