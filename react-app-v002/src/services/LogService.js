export async function getAllLog() {
    const response = await fetch('http://localhost:8080/api/v1/users/logs/all');
    return await response.json();
}

export async function createLog(data){
    console.log(JSON.stringify(data));
    const response = await fetch('http://localhost:8080/api/v1/users/logs/create', {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(data)
    })

    return await response.json();
}