export async function getAllUsers() {
    const response = await fetch('http://localhost:8080/api/v1/users');
    return await response.json();
}

export async function deleteUser(userId) {
    const response = await fetch(`http://localhost:8080/api/v1/users/${userId}`, {
        method: 'DELETE',
        headers: {'Content-Type': 'application/json'}
    })
    return await response.json();
}

export async function updateUser(user) {
    const response = await fetch(`http://localhost:8080/api/v1/users/${user.id_user}`, {
        method: 'PUT',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(user)
    })
    return await response.json();
}

export async function getSearchUsers(params) {
    const response = await fetch(`http://localhost:8080/api/v1/users/name/${params}`);
    return await response.json();
}

export async function createUser(data){
    console.log(JSON.stringify(data));
    const response = await fetch('http://localhost:8080/api/v1/users', {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(data)
    })

    return await response.json();
}