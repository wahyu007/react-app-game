export async function getAllGame() {
    const response = await fetch('http://localhost:8080/api/v1/users/games/all');
    return await response.json();
}

export async function createGame(data){
    console.log(JSON.stringify(data));
    const response = await fetch('http://localhost:8080/api/v1/users/games', {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(data)
    })

    return await response.json();
}