import React from 'react';
import {NavLink} from 'react-router-dom';
import { Navbar, NavDropdown, Nav} from 'react-bootstrap';

export const Header = () => {
    return(
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
            <Navbar.Brand href="#">Navbar</Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
                <Nav className="mx-auto">
                    <NavLink className="nav-link" to="/game">Game</NavLink>
                    <NavLink className="nav-link" to="/user">User</NavLink>
                    <NavLink className="nav-link" to="/history">History</NavLink>
                </Nav>
                <Nav>
                    <NavDropdown title="Admin" id="collasible-nav-dropdown">
                        <NavDropdown.Item href="#">Profile</NavDropdown.Item>
                        <NavDropdown.Divider />
                        <NavDropdown.Item href="#">Logout</NavDropdown.Item>
                    </NavDropdown>
                </Nav>
            </Navbar.Collapse>
            <Nav>
                
            </Nav>
        </Navbar>
    )
}

export default Header;