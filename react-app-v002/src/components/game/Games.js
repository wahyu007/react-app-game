import React from 'react';

export const Games = ({ games }) => {
    if(games.length === 0) return null

    const GameRow = (user, index) => {
        return(
            <tr key={ index } className={index%2 === 0? 'odd':'even'}>
                <td>{index + 1}</td>
                <td>{user.id_user}</td>
                <td>{user.score}</td>
                <td>{user.top_score}</td>
            </tr>
        )
    }

    const userTable = games.map((user, index) => GameRow(user, index))

    return(
        <div className="container">
            <h2>Games Data</h2>
            <table className="table table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>ID User</th>
                        <th>Score</th>
                        <th>Top Score</th>
                    </tr>
                </thead>
                <tbody>
                    {userTable}
                </tbody>
            </table>
        </div>
    )
}

export default Games;