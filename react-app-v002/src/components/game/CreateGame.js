import React from 'react';
import {Row, Col, Button} from 'react-bootstrap';

const CreateGame = ({ onChangeForm, createGame}) => {
    return(
        <div className="text-left">
            <Row>
                <Col className="form-group" md={4}>
                    <label htmlFor="exampleInputEmail1">Id User</label>
                    <input type="text" onChange={ (e) => onChangeForm(e) } className="form-control" name="name" aria-describedby="emailHelp" placeholder="Name" />
                </Col>
                <Col className="form-group" md={4}>
                    <label>Score</label>
                    <input type="number" onChange={ (e) => onChangeForm(e) } className="form-control" name="score" aria-describedby="scoreHelp" placeholder="Score" />                    
                </Col>
                <Col className="form-group" md={4}>
                    <label>Top Score</label>
                    <input type="number" onChange={ (e) => onChangeForm(e) } className="form-control" name="topScore" aria-describedby="topScoreHelp" placeholder="Top Score" />                    
                </Col>
            </Row>
            <Button type="button" onClick={(e) => createGame() } className="btn btn-danger">Create</Button>
        </div>
    )
}

export default CreateGame;