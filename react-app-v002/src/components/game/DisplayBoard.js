import React from 'react';

export const DisplayBoard = ({ numberOfGames, getAllGame }) => {
    return(
        <div className="display-board">
            <h4>Game Created</h4>
            <div className="number">
                {numberOfGames}
            </div>
            <div className="btn">
                <button type="button" onClick={() => getAllGame()} className="btn btn-warning">Get all Game</button>
            </div>
        </div>
    )
}

export default DisplayBoard;