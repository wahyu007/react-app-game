import React, { Component } from 'react'
import {Container, Row, Col} from 'react-bootstrap'
import Games from './Games';
import CreateGame from './CreateGame';
import DisplayBoard from './DisplayBoard';
import { getAllGame, createGame } from './../../services/GameService';

class Game extends Component {
    state = {
        games: [],
        game: {},
        numberOfGames: 0,
        data: 0,
    }

    componentDidMount() {
        console.log('Component did mount');
    }

    getAllGames = () => {
        getAllGame()
        .then(games => {
            console.log(games)
            this.setState({ games: games, numberOfGames: games.length})
        })
    }

    onChangeForm = (e) => {
        let game = this.state.game;
        if(e.target.name === 'name' ){
          game.name = e.target.value
        } else if ( e.target.name === 'email' ){
          game.email = e.target.value;
        } else if(e.target.name === 'password'){
          game.password = e.target.value
        } else if(e.target.name === 'sex'){
          game.sex = e.target.value
        } else if(e.target.name === 'address'){
          game.address = e.target.value
        } else if(e.target.name === 'phone'){
          game.phone = e.target.value
        }
    
        console.log(game)
        this.setState({game})
    }

    setNewNumber = () => {
        this.setState({ data: this.state.data + 1})
    }

    createGame = () => {
        console.log(`create user ${this.state.user}`)
        createGame(this.state.user)
          .then(response => {
            console.log(response);
            this.setState({ numberOfGames: this.state.numberOfGames + 1})
          });
    }

    render(){
        return (
            <div>
                <Container>
                    <Row className="mrgnbtm">
                        <Col md={8}>
                            <div>
                                <h1>Operate Game Data</h1>
                            </div>
                            <CreateGame
                                game={this.state.game}
                                onChangeForm={this.onChangeForm}
                                createGame = {this.createGame}
                            ></CreateGame>
                        </Col>
                        <Col md={4}>
                            <DisplayBoard
                                data={this.state.data}
                                setNewNumber={this.setNewNumber}
                                numberOfGames={this.state.numberOfGames}
                                getAllGame={this.getAllGames}
                            ></DisplayBoard>
                        </Col>
                    </Row>
                    <Row className="mrgnbtm">
                        <Games 
                            games={this.state.games}
                        >
                        </Games>
                    </Row>
                </Container>
            </div>
        )
    }
}

export default Game;