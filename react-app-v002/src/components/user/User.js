import React, { Component } from 'react'
import {Container, Row, Col } from 'react-bootstrap'
import DisplayBoard from './DisplayBoard';
import CreateUser from './CreateUser';
import EditUser from './EditUser';
import Users from './Users';
import { getAllUsers, createUser, getSearchUsers, deleteUser, updateUser} from './../../services/UserService';

class User extends Component {
    state = {
        user : {},
        users: [],
        numberOfUsers: 0,
        search:'',
        show: false,
        editValue: {},
        editUser: {},
        data: 0,
    }
    
    getAllUsers = () => {
        getAllUsers()
        .then(users => {
            console.log(users)
            this.setState({ users: users, numberOfUsers: users.length})
        })
    }
    
    setNewNumber = () => {
        this.setState({ data: this.state.data + 1})
    }
    
    handleClose = () => {
        this.setState({ show: false});
        this.getAllUsers();
    };
    
    createUsers = (e) => {
        console.log(`create user ${this.state.user}`)
        createUser(this.state.user)
          .then(response => {
            console.log(response);
            this.setState({ numberOfUsers: this.state.numberOfUsers + 1})
          });
    }
    
    updateUser = () => {
        console.log(`update user ${this.state.user}`)
        updateUser(this.state.editValue)
          .then(response => {
            console.log(response);
            this.setState({ show: false})
          });
    }
    
    getUser = (user) => {
        this.setState({ show: true, editValue: user});
        console.log(user);
    }
    
    getSearch = () => {
        if(this.state.search === ''){
          getAllUsers();
        } else {
          getSearchUsers(this.state.search)
          .then(users => {
            if(users.length <= 0) {
              this.setState({ users: [{name: "null"}]})
            } else {
              this.setState({ users: users})
            }
          })
        }
    }
    
    onchangeEdit = (e) => {
        let editValue = this.state.editValue;
        if(e.target.name === 'name' ){
          editValue.name = e.target.value
        } else if ( e.target.name === 'email' ){
          editValue.email = e.target.value;
        } else if(e.target.name === 'newPassword'){
          editValue.newPassword = e.target.value
        } else if(e.target.name === 'sex'){
          editValue.sex = e.target.value
        } else if(e.target.name === 'address'){
          editValue.address = e.target.value
        } else if(e.target.name === 'phone'){
          editValue.phone = e.target.value
        }
    
        console.log(editValue)
        this.setState({editValue})
    }
    
    onChangeForm = (e) => {
        let user = this.state.user;
        if(e.target.name === 'name' ){
          user.name = e.target.value
        } else if ( e.target.name === 'email' ){
          user.email = e.target.value;
        } else if(e.target.name === 'password'){
          user.password = e.target.value
        } else if(e.target.name === 'sex'){
          user.sex = e.target.value
        } else if(e.target.name === 'address'){
          user.address = e.target.value
        } else if(e.target.name === 'phone'){
          user.phone = e.target.value
        }
    
        console.log(user)
        this.setState({user})
    }
    
    onSearchForm = (e) => {
        console.log(e.target.value);
        this.setState({search: e.target.value});
    }
    
    deleteUser = (userId) => {
        deleteUser(userId)
        .then(user => {
          console.log(user);
          this.getAllUsers();
        })
    }

    render() {
        return (
            <div>
                <Container>
                    <Row className="mrgnbtm">
                        <Col md={8}>
                            <div>
                              <h1>Operate User Data</h1>
                            </div>
                            <CreateUser
                                user={this.state.user}
                                onChangeForm={this.onChangeForm}
                                createUser = {this.createUsers}
                            ></CreateUser>
                        </Col>
                        <Col md={4}>
                            <DisplayBoard
                                data={this.state.data}
                                setNewNumber={this.setNewNumber}
                                numberOfUsers={this.state.numberOfUsers}
                                getAllUsers={this.getAllUsers}
                            ></DisplayBoard>
                        </Col>
                    </Row>
                    <Row className="mrgnbtm">
                        <Users 
                        users={this.state.users}
                        onSearchForm={this.onSearchForm} 
                        getSearch={this.getSearch}
                        getUser={this.getUser}
                        deleteUser={this.deleteUser}
                        >
                        </Users>
                    </Row>
                </Container>
                <EditUser
                    onchangeEdit={this.onchangeEdit}
                    show={this.state.show}
                    editValue={this.state.editValue}
                    handleClose={this.handleClose}
                    updateUser={this.updateUser}
                    >
                </EditUser>
            </div>
        )
    }
}

export default User;