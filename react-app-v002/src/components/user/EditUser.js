import React from 'react';
import {Button, Col, Container, Form, Modal, Row} from 'react-bootstrap';

export const EditUser = ({show, editValue, handleClose, onchangeEdit, updateUser}) => {
    
    return(
        <div>
            <Modal show={show} onHide={() => handleClose()}>
                <Modal.Header closeButton>
                    <Modal.Title>Edit User</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Container>
                        <Row>
                            <Col md={4} className="form-group">
                                <Form.Label>Name</Form.Label>
                                <Form.Control type="text" name="name" value={editValue.name} onChange={ (e) => onchangeEdit(e) } />
                            </Col>
                            <Col md={4} className="form-group">
                                <Form.Label>Email</Form.Label>
                                <Form.Control type="email" name="email"  value={editValue.email} onChange={ (e) => onchangeEdit(e) } />
                            </Col>
                            <Col md={4} className="form-group">
                                <Form.Label>New Password</Form.Label>
                                <Form.Control type="password" name="newPassword" onChange={ (e) => onchangeEdit(e) } placeholder="New Password"/>
                            </Col>
                        </Row>
                        <Row>
                            <Col md={4} className="form-group">
                                <Form.Label>Address</Form.Label>
                                <Form.Control type="text" name="address"  value={editValue.address} onChange={ (e) => onchangeEdit(e) } />
                            </Col>
                            <Col md={4} className="form-group">
                                <Form.Label>Phone</Form.Label>
                                <Form.Control type="tel" name="phone"  value={editValue.phone} onChange={ (e) => onchangeEdit(e) } />
                            </Col>
                            <Col md={4} className="form-group">
                                <Form.Group>
                                    <Form.Label>Sex</Form.Label>
                                    <Form.Control as="select" type="text" name="sex" value={editValue.sex} onChange={ (e) => onchangeEdit(e) }>
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                    </Form.Control>
                                </Form.Group>
                            </Col>
                        </Row>
                    </Container>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={() => updateUser()}>
                        Update
                    </Button>
                </Modal.Footer>
            </Modal>
        </div>
        
    )
}

export default EditUser;