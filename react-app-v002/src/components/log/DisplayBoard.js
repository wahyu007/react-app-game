import React from 'react';

export const DisplayBoard = ({ numberOfLogs, getAllLogs }) => {
    return(
        <div className="display-board">
            <h4>Log Created</h4>
            <div className="number">
                {numberOfLogs}
            </div>
            <div className="btn">
                <button type="button" onClick={() => getAllLogs()} className="btn btn-warning">Get all Log</button>
            </div>
        </div>
    )
}

export default DisplayBoard;