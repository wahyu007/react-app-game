import React from 'react';
import {Row, Col, Button} from 'react-bootstrap';

const CreateLog = ({ onChangeForm, createLog}) => {
    return(
        <div className="text-left">
            <Row>
                <Col className="form-group" md={4}>
                    <label htmlFor="exampleInputEmail1">Id User</label>
                    <input type="number" onChange={ (e) => onChangeForm(e) } className="form-control" name="id_user" aria-describedby="emailHelp" placeholder="ID User" />
                </Col>
                <Col className="form-group" md={4}>
                    <label>Status</label>
                    <input type="text" onChange={ (e) => onChangeForm(e) } className="form-control" name="status" aria-describedby="scoreHelp" placeholder="Status" />                    
                </Col>
            </Row>
            <Button type="button" onClick={(e) => createLog() } className="btn btn-danger">Create</Button>
        </div>
    )
}

export default CreateLog;