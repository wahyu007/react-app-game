import React from 'react';

export const Logs = ({ logs }) => {
    if(logs.length === 0) return null

    const LogRow = (log, index) => {
        return(
            <tr key={ index } className={index%2 === 0? 'odd':'even'}>
                <td>{index + 1}</td>
                <td>{log.id_user}</td>
                <td>{log.status}</td>
            </tr>
        )
    }

    const logTable = logs.map((log, index) => LogRow(log, index))

    return(
        <div className="container">
            <h2>Logs Data</h2>
            <table className="table table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>ID User</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {logTable}
                </tbody>
            </table>
        </div>
    )
}

export default Logs;