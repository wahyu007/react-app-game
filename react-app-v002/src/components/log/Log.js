import React, { Component } from 'react'
import Logs from './Logs';
import DisplayBoard from './DisplayBoard';
import CreateLog from './CreateLog';
import { createLog, getAllLog} from './../../services/LogService';
import {Container, Row, Col } from 'react-bootstrap'

class Log extends Component {
    state = {
        logs: [],
        log: {},
        numberOfLogs: 0,
        data: 0
    }

    componentDidMount() {
        console.log('Component did mount');
    }

    getAllLogs = () => {
        getAllLog()
        .then(logs => {
            console.log(logs)
            this.setState({ logs: logs, numberOflogs: logs.length})
        })
    }

    onChangeForm = (e) => {
        let log = this.state.log;
        if(e.target.name === 'id_user' ){
          log.id_user = e.target.value
        } else if ( e.target.name === 'status' ){
          log.status = e.target.value;
        }
    
        console.log(log)
        this.setState({log})
    }

    setNewNumber = () => {
        this.setState({ data: this.state.data + 1})
    }

    createLog = () => {
        console.log(`create user ${this.state.log}`)
        createLog(this.state.log)
          .then(response => {
            console.log(response);
            this.setState({ numberOfLogs: this.state.numberOfLogs + 1})
          });
    }

    render() {
        return (
            <div>
                <Container>
                    <Row className="mrgnbtm">
                        <Col md={8}>
                            <div>
                                <h1>Operate Log Data</h1>
                            </div>
                            <CreateLog
                                log={this.state.log}
                                onChangeForm={this.onChangeForm}
                                createLog = {this.createLog}
                            ></CreateLog>
                        </Col>
                        <Col md={4}>
                            <DisplayBoard
                                data={this.state.data}
                                setNewNumber={this.setNewNumber}
                                numberOfLogs={this.state.numberOfLogs}
                                getAllLogs={this.getAllLogs}
                            ></DisplayBoard>
                        </Col>
                    </Row>
                    <Row className="mrgnbtm">
                        <Logs 
                            logs={this.state.logs}
                        >
                        </Logs>
                    </Row>
                </Container>
            </div>
        )
    }
}

export default Log;